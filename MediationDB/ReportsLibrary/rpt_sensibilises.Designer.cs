﻿namespace MediationDB.ReportsLibrary
{
    partial class rpt_sensibilises
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.DataAccess.Sql.StoredProcQuery storedProcQuery1 = new DevExpress.DataAccess.Sql.StoredProcQuery();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpt_sensibilises));
            DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary11 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary12 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary13 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary14 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary15 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary16 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary17 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary18 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary19 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary20 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary21 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary22 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary23 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary24 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary25 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary26 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary27 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary28 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary29 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary30 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary31 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary32 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary33 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary34 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary35 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary36 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary37 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary38 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary39 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary40 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary41 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary42 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary43 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary44 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary45 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary46 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary47 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary48 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary49 = new DevExpress.XtraReports.UI.XRSummary();
            DevExpress.XtraReports.UI.XRSummary xrSummary50 = new DevExpress.XtraReports.UI.XRSummary();
            this.sqlDataSource1 = new DevExpress.DataAccess.Sql.SqlDataSource(this.components);
            this.Title = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupCaption1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupData1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailCaption1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailData1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GroupFooterBackground3 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.DetailData3_Odd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.TotalCaption1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.TotalData1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.TotalBackground1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GrandTotalCaption1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GrandTotalData1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.GrandTotalBackground1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.PageInfo = new DevExpress.XtraReports.UI.XRControlStyle();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupHeader2 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupHeader3 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupHeader4 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.GroupHeader5 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.GroupFooter1 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.GroupFooter2 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.GroupFooter3 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.GroupFooter4 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.GroupFooter5 = new DevExpress.XtraReports.UI.GroupFooterBand();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.pageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.pageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.label1 = new DevExpress.XtraReports.UI.XRLabel();
            this.table1 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.table2 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.table3 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.table4 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.table5 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.table6 = new DevExpress.XtraReports.UI.XRTable();
            this.tableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.tableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.tableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.label2 = new DevExpress.XtraReports.UI.XRLabel();
            this.panel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.label3 = new DevExpress.XtraReports.UI.XRLabel();
            this.label4 = new DevExpress.XtraReports.UI.XRLabel();
            this.label5 = new DevExpress.XtraReports.UI.XRLabel();
            this.label6 = new DevExpress.XtraReports.UI.XRLabel();
            this.label7 = new DevExpress.XtraReports.UI.XRLabel();
            this.label8 = new DevExpress.XtraReports.UI.XRLabel();
            this.label9 = new DevExpress.XtraReports.UI.XRLabel();
            this.label10 = new DevExpress.XtraReports.UI.XRLabel();
            this.label11 = new DevExpress.XtraReports.UI.XRLabel();
            this.label12 = new DevExpress.XtraReports.UI.XRLabel();
            this.label13 = new DevExpress.XtraReports.UI.XRLabel();
            this.label14 = new DevExpress.XtraReports.UI.XRLabel();
            this.label15 = new DevExpress.XtraReports.UI.XRLabel();
            this.label16 = new DevExpress.XtraReports.UI.XRLabel();
            this.label17 = new DevExpress.XtraReports.UI.XRLabel();
            this.label18 = new DevExpress.XtraReports.UI.XRLabel();
            this.label19 = new DevExpress.XtraReports.UI.XRLabel();
            this.label20 = new DevExpress.XtraReports.UI.XRLabel();
            this.label21 = new DevExpress.XtraReports.UI.XRLabel();
            this.label22 = new DevExpress.XtraReports.UI.XRLabel();
            this.label23 = new DevExpress.XtraReports.UI.XRLabel();
            this.panel2 = new DevExpress.XtraReports.UI.XRPanel();
            this.label24 = new DevExpress.XtraReports.UI.XRLabel();
            this.label25 = new DevExpress.XtraReports.UI.XRLabel();
            this.label26 = new DevExpress.XtraReports.UI.XRLabel();
            this.label27 = new DevExpress.XtraReports.UI.XRLabel();
            this.label28 = new DevExpress.XtraReports.UI.XRLabel();
            this.label29 = new DevExpress.XtraReports.UI.XRLabel();
            this.label30 = new DevExpress.XtraReports.UI.XRLabel();
            this.label31 = new DevExpress.XtraReports.UI.XRLabel();
            this.label32 = new DevExpress.XtraReports.UI.XRLabel();
            this.label33 = new DevExpress.XtraReports.UI.XRLabel();
            this.label34 = new DevExpress.XtraReports.UI.XRLabel();
            this.label35 = new DevExpress.XtraReports.UI.XRLabel();
            this.label36 = new DevExpress.XtraReports.UI.XRLabel();
            this.label37 = new DevExpress.XtraReports.UI.XRLabel();
            this.label38 = new DevExpress.XtraReports.UI.XRLabel();
            this.label39 = new DevExpress.XtraReports.UI.XRLabel();
            this.label40 = new DevExpress.XtraReports.UI.XRLabel();
            this.label41 = new DevExpress.XtraReports.UI.XRLabel();
            this.label42 = new DevExpress.XtraReports.UI.XRLabel();
            this.label43 = new DevExpress.XtraReports.UI.XRLabel();
            this.label44 = new DevExpress.XtraReports.UI.XRLabel();
            this.panel3 = new DevExpress.XtraReports.UI.XRPanel();
            this.label45 = new DevExpress.XtraReports.UI.XRLabel();
            this.label46 = new DevExpress.XtraReports.UI.XRLabel();
            this.label47 = new DevExpress.XtraReports.UI.XRLabel();
            this.label48 = new DevExpress.XtraReports.UI.XRLabel();
            this.label49 = new DevExpress.XtraReports.UI.XRLabel();
            this.label50 = new DevExpress.XtraReports.UI.XRLabel();
            this.label51 = new DevExpress.XtraReports.UI.XRLabel();
            this.label52 = new DevExpress.XtraReports.UI.XRLabel();
            this.label53 = new DevExpress.XtraReports.UI.XRLabel();
            this.label54 = new DevExpress.XtraReports.UI.XRLabel();
            this.label55 = new DevExpress.XtraReports.UI.XRLabel();
            this.label56 = new DevExpress.XtraReports.UI.XRLabel();
            this.label57 = new DevExpress.XtraReports.UI.XRLabel();
            this.label58 = new DevExpress.XtraReports.UI.XRLabel();
            this.label59 = new DevExpress.XtraReports.UI.XRLabel();
            this.label60 = new DevExpress.XtraReports.UI.XRLabel();
            this.label61 = new DevExpress.XtraReports.UI.XRLabel();
            this.label62 = new DevExpress.XtraReports.UI.XRLabel();
            this.label63 = new DevExpress.XtraReports.UI.XRLabel();
            this.label64 = new DevExpress.XtraReports.UI.XRLabel();
            this.label65 = new DevExpress.XtraReports.UI.XRLabel();
            this.panel4 = new DevExpress.XtraReports.UI.XRPanel();
            this.label66 = new DevExpress.XtraReports.UI.XRLabel();
            this.label67 = new DevExpress.XtraReports.UI.XRLabel();
            this.label68 = new DevExpress.XtraReports.UI.XRLabel();
            this.label69 = new DevExpress.XtraReports.UI.XRLabel();
            this.label70 = new DevExpress.XtraReports.UI.XRLabel();
            this.label71 = new DevExpress.XtraReports.UI.XRLabel();
            this.label72 = new DevExpress.XtraReports.UI.XRLabel();
            this.label73 = new DevExpress.XtraReports.UI.XRLabel();
            this.label74 = new DevExpress.XtraReports.UI.XRLabel();
            this.label75 = new DevExpress.XtraReports.UI.XRLabel();
            this.label76 = new DevExpress.XtraReports.UI.XRLabel();
            this.label77 = new DevExpress.XtraReports.UI.XRLabel();
            this.label78 = new DevExpress.XtraReports.UI.XRLabel();
            this.label79 = new DevExpress.XtraReports.UI.XRLabel();
            this.label80 = new DevExpress.XtraReports.UI.XRLabel();
            this.label81 = new DevExpress.XtraReports.UI.XRLabel();
            this.label82 = new DevExpress.XtraReports.UI.XRLabel();
            this.label83 = new DevExpress.XtraReports.UI.XRLabel();
            this.label84 = new DevExpress.XtraReports.UI.XRLabel();
            this.label85 = new DevExpress.XtraReports.UI.XRLabel();
            this.label86 = new DevExpress.XtraReports.UI.XRLabel();
            this.panel5 = new DevExpress.XtraReports.UI.XRPanel();
            this.label87 = new DevExpress.XtraReports.UI.XRLabel();
            this.label88 = new DevExpress.XtraReports.UI.XRLabel();
            this.label89 = new DevExpress.XtraReports.UI.XRLabel();
            this.label90 = new DevExpress.XtraReports.UI.XRLabel();
            this.label91 = new DevExpress.XtraReports.UI.XRLabel();
            this.label92 = new DevExpress.XtraReports.UI.XRLabel();
            this.label93 = new DevExpress.XtraReports.UI.XRLabel();
            this.label94 = new DevExpress.XtraReports.UI.XRLabel();
            this.label95 = new DevExpress.XtraReports.UI.XRLabel();
            this.label96 = new DevExpress.XtraReports.UI.XRLabel();
            this.label97 = new DevExpress.XtraReports.UI.XRLabel();
            this.label98 = new DevExpress.XtraReports.UI.XRLabel();
            this.label99 = new DevExpress.XtraReports.UI.XRLabel();
            this.label100 = new DevExpress.XtraReports.UI.XRLabel();
            this.label101 = new DevExpress.XtraReports.UI.XRLabel();
            this.label102 = new DevExpress.XtraReports.UI.XRLabel();
            this.label103 = new DevExpress.XtraReports.UI.XRLabel();
            this.label104 = new DevExpress.XtraReports.UI.XRLabel();
            this.label105 = new DevExpress.XtraReports.UI.XRLabel();
            this.label106 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.table1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.table6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.ConnectionName = "localhost_db_foncier_serveurextrn";
            this.sqlDataSource1.Name = "sqlDataSource1";
            storedProcQuery1.Name = "liste_sensibilises";
            storedProcQuery1.StoredProcName = "liste_sensibilises";
            this.sqlDataSource1.Queries.AddRange(new DevExpress.DataAccess.Sql.SqlQuery[] {
            storedProcQuery1});
            this.sqlDataSource1.ResultSchemaSerializable = resources.GetString("sqlDataSource1.ResultSchemaSerializable");
            // 
            // Title
            // 
            this.Title.BackColor = System.Drawing.Color.Transparent;
            this.Title.BorderColor = System.Drawing.Color.Black;
            this.Title.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.Title.BorderWidth = 1F;
            this.Title.Font = new System.Drawing.Font("Arial", 14.25F);
            this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
            this.Title.Name = "Title";
            // 
            // GroupCaption1
            // 
            this.GroupCaption1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(104)))), ((int)(((byte)(196)))));
            this.GroupCaption1.BorderColor = System.Drawing.Color.White;
            this.GroupCaption1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupCaption1.BorderWidth = 2F;
            this.GroupCaption1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.GroupCaption1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(228)))), ((int)(((byte)(228)))));
            this.GroupCaption1.Name = "GroupCaption1";
            this.GroupCaption1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupCaption1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupData1
            // 
            this.GroupData1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(104)))), ((int)(((byte)(196)))));
            this.GroupData1.BorderColor = System.Drawing.Color.White;
            this.GroupData1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupData1.BorderWidth = 2F;
            this.GroupData1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.GroupData1.ForeColor = System.Drawing.Color.White;
            this.GroupData1.Name = "GroupData1";
            this.GroupData1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupData1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailCaption1
            // 
            this.DetailCaption1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(104)))), ((int)(((byte)(196)))));
            this.DetailCaption1.BorderColor = System.Drawing.Color.White;
            this.DetailCaption1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.DetailCaption1.BorderWidth = 2F;
            this.DetailCaption1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.DetailCaption1.ForeColor = System.Drawing.Color.White;
            this.DetailCaption1.Name = "DetailCaption1";
            this.DetailCaption1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
            this.DetailCaption1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailData1
            // 
            this.DetailData1.BorderColor = System.Drawing.Color.Transparent;
            this.DetailData1.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.DetailData1.BorderWidth = 2F;
            this.DetailData1.Font = new System.Drawing.Font("Arial", 8.25F);
            this.DetailData1.ForeColor = System.Drawing.Color.Black;
            this.DetailData1.Name = "DetailData1";
            this.DetailData1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
            this.DetailData1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GroupFooterBackground3
            // 
            this.GroupFooterBackground3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(117)))), ((int)(((byte)(129)))));
            this.GroupFooterBackground3.BorderColor = System.Drawing.Color.White;
            this.GroupFooterBackground3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GroupFooterBackground3.BorderWidth = 2F;
            this.GroupFooterBackground3.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.GroupFooterBackground3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(228)))), ((int)(((byte)(228)))));
            this.GroupFooterBackground3.Name = "GroupFooterBackground3";
            this.GroupFooterBackground3.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GroupFooterBackground3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // DetailData3_Odd
            // 
            this.DetailData3_Odd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.DetailData3_Odd.BorderColor = System.Drawing.Color.Transparent;
            this.DetailData3_Odd.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.DetailData3_Odd.BorderWidth = 1F;
            this.DetailData3_Odd.Font = new System.Drawing.Font("Arial", 8.25F);
            this.DetailData3_Odd.ForeColor = System.Drawing.Color.Black;
            this.DetailData3_Odd.Name = "DetailData3_Odd";
            this.DetailData3_Odd.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 6, 0, 0, 100F);
            this.DetailData3_Odd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TotalCaption1
            // 
            this.TotalCaption1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TotalCaption1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.TotalCaption1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(186)))), ((int)(((byte)(192)))));
            this.TotalCaption1.Name = "TotalCaption1";
            this.TotalCaption1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.TotalCaption1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TotalData1
            // 
            this.TotalData1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.TotalData1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.TotalData1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.TotalData1.Name = "TotalData1";
            this.TotalData1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 6, 0, 0, 100F);
            this.TotalData1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // TotalBackground1
            // 
            this.TotalBackground1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(245)))), ((int)(((byte)(248)))));
            this.TotalBackground1.BorderColor = System.Drawing.Color.White;
            this.TotalBackground1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.TotalBackground1.BorderWidth = 2F;
            this.TotalBackground1.Name = "TotalBackground1";
            // 
            // GrandTotalCaption1
            // 
            this.GrandTotalCaption1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.GrandTotalCaption1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.GrandTotalCaption1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(147)))), ((int)(((byte)(147)))), ((int)(((byte)(147)))));
            this.GrandTotalCaption1.Name = "GrandTotalCaption1";
            this.GrandTotalCaption1.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.GrandTotalCaption1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GrandTotalData1
            // 
            this.GrandTotalData1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.GrandTotalData1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.GrandTotalData1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.GrandTotalData1.Name = "GrandTotalData1";
            this.GrandTotalData1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 6, 0, 0, 100F);
            this.GrandTotalData1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // GrandTotalBackground1
            // 
            this.GrandTotalBackground1.BackColor = System.Drawing.Color.White;
            this.GrandTotalBackground1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(75)))), ((int)(((byte)(75)))));
            this.GrandTotalBackground1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.GrandTotalBackground1.BorderWidth = 2F;
            this.GrandTotalBackground1.Name = "GrandTotalBackground1";
            // 
            // PageInfo
            // 
            this.PageInfo.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.PageInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(70)))), ((int)(((byte)(80)))));
            this.PageInfo.Name = "PageInfo";
            this.PageInfo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            // 
            // TopMargin
            // 
            this.TopMargin.Name = "TopMargin";
            // 
            // BottomMargin
            // 
            this.BottomMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.pageInfo1,
            this.pageInfo2});
            this.BottomMargin.HeightF = 29F;
            this.BottomMargin.Name = "BottomMargin";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label1});
            this.ReportHeader.HeightF = 60F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table1});
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("id_localite", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader1.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader1.HeightF = 27F;
            this.GroupHeader1.Level = 1;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // GroupHeader2
            // 
            this.GroupHeader2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table2});
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("id_groupement", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader2.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader2.HeightF = 27F;
            this.GroupHeader2.Level = 2;
            this.GroupHeader2.Name = "GroupHeader2";
            // 
            // GroupHeader3
            // 
            this.GroupHeader3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table3});
            this.GroupHeader3.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("id_chefferie", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader3.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader3.HeightF = 27F;
            this.GroupHeader3.Level = 3;
            this.GroupHeader3.Name = "GroupHeader3";
            // 
            // GroupHeader4
            // 
            this.GroupHeader4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table4});
            this.GroupHeader4.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("id_territoire", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            this.GroupHeader4.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader4.HeightF = 27F;
            this.GroupHeader4.Level = 4;
            this.GroupHeader4.Name = "GroupHeader4";
            // 
            // GroupHeader5
            // 
            this.GroupHeader5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table5});
            this.GroupHeader5.GroupUnion = DevExpress.XtraReports.UI.GroupUnion.WithFirstDetail;
            this.GroupHeader5.HeightF = 28F;
            this.GroupHeader5.Level = 5;
            this.GroupHeader5.Name = "GroupHeader5";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.table6});
            this.Detail.HeightF = 25F;
            this.Detail.Name = "Detail";
            // 
            // GroupFooter1
            // 
            this.GroupFooter1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label2});
            this.GroupFooter1.GroupUnion = DevExpress.XtraReports.UI.GroupFooterUnion.WithLastDetail;
            this.GroupFooter1.HeightF = 6F;
            this.GroupFooter1.Name = "GroupFooter1";
            // 
            // GroupFooter2
            // 
            this.GroupFooter2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panel1});
            this.GroupFooter2.HeightF = 56.16888F;
            this.GroupFooter2.Level = 1;
            this.GroupFooter2.Name = "GroupFooter2";
            // 
            // GroupFooter3
            // 
            this.GroupFooter3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panel2});
            this.GroupFooter3.HeightF = 56.16888F;
            this.GroupFooter3.Level = 2;
            this.GroupFooter3.Name = "GroupFooter3";
            // 
            // GroupFooter4
            // 
            this.GroupFooter4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panel3});
            this.GroupFooter4.HeightF = 56.16888F;
            this.GroupFooter4.Level = 3;
            this.GroupFooter4.Name = "GroupFooter4";
            // 
            // GroupFooter5
            // 
            this.GroupFooter5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panel4});
            this.GroupFooter5.HeightF = 56.16888F;
            this.GroupFooter5.Level = 4;
            this.GroupFooter5.Name = "GroupFooter5";
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.panel5});
            this.ReportFooter.HeightF = 49.38444F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // pageInfo1
            // 
            this.pageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(6F, 6F);
            this.pageInfo1.Name = "pageInfo1";
            this.pageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
            this.pageInfo1.SizeF = new System.Drawing.SizeF(572.5F, 23F);
            this.pageInfo1.StyleName = "PageInfo";
            // 
            // pageInfo2
            // 
            this.pageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(590F, 6F);
            this.pageInfo2.Name = "pageInfo2";
            this.pageInfo2.SizeF = new System.Drawing.SizeF(572.5F, 23F);
            this.pageInfo2.StyleName = "PageInfo";
            this.pageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.pageInfo2.TextFormatString = "Page {0} of {1}";
            // 
            // label1
            // 
            this.label1.LocationFloat = new DevExpress.Utils.PointFloat(6F, 6F);
            this.label1.Name = "label1";
            this.label1.SizeF = new System.Drawing.SizeF(1157F, 24.19433F);
            this.label1.StyleName = "Title";
            this.label1.Text = "Rapport de sensibilisations";
            // 
            // table1
            // 
            this.table1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.table1.Name = "table1";
            this.table1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow1});
            this.table1.SizeF = new System.Drawing.SizeF(1169F, 25F);
            // 
            // tableRow1
            // 
            this.tableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell1,
            this.tableCell2});
            this.tableRow1.Name = "tableRow1";
            this.tableRow1.Weight = 1D;
            // 
            // tableCell1
            // 
            this.tableCell1.Name = "tableCell1";
            this.tableCell1.StyleName = "GroupCaption1";
            this.tableCell1.Text = "ID LOCALITE";
            this.tableCell1.Weight = 0.068372871653456485D;
            // 
            // tableCell2
            // 
            this.tableCell2.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[id_localite]")});
            this.tableCell2.Name = "tableCell2";
            this.tableCell2.StyleName = "GroupData1";
            this.tableCell2.Weight = 0.93162715445225619D;
            // 
            // table2
            // 
            this.table2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.table2.Name = "table2";
            this.table2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow2});
            this.table2.SizeF = new System.Drawing.SizeF(1169F, 25F);
            // 
            // tableRow2
            // 
            this.tableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell3,
            this.tableCell4});
            this.tableRow2.Name = "tableRow2";
            this.tableRow2.Weight = 1D;
            // 
            // tableCell3
            // 
            this.tableCell3.Name = "tableCell3";
            this.tableCell3.StyleName = "GroupCaption1";
            this.tableCell3.Text = "ID GROUPEMENT";
            this.tableCell3.Weight = 0.089618124851719549D;
            // 
            // tableCell4
            // 
            this.tableCell4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[id_groupement]")});
            this.tableCell4.Name = "tableCell4";
            this.tableCell4.StyleName = "GroupData1";
            this.tableCell4.Weight = 0.91038190778042127D;
            // 
            // table3
            // 
            this.table3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.table3.Name = "table3";
            this.table3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow3});
            this.table3.SizeF = new System.Drawing.SizeF(1169F, 25F);
            // 
            // tableRow3
            // 
            this.tableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell5,
            this.tableCell6});
            this.tableRow3.Name = "tableRow3";
            this.tableRow3.Weight = 1D;
            // 
            // tableCell5
            // 
            this.tableCell5.Name = "tableCell5";
            this.tableCell5.StyleName = "GroupCaption1";
            this.tableCell5.Text = "ID CHEFFERIE";
            this.tableCell5.Weight = 0.074915400522197792D;
            // 
            // tableCell6
            // 
            this.tableCell6.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[id_chefferie]")});
            this.tableCell6.Name = "tableCell6";
            this.tableCell6.StyleName = "GroupData1";
            this.tableCell6.Weight = 0.92508464516279942D;
            // 
            // table4
            // 
            this.table4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.table4.Name = "table4";
            this.table4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow4});
            this.table4.SizeF = new System.Drawing.SizeF(1169F, 25F);
            // 
            // tableRow4
            // 
            this.tableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell7,
            this.tableCell8});
            this.tableRow4.Name = "tableRow4";
            this.tableRow4.Weight = 1D;
            // 
            // tableCell7
            // 
            this.tableCell7.Name = "tableCell7";
            this.tableCell7.StyleName = "GroupCaption1";
            this.tableCell7.Text = "ID TERRITOIRE";
            this.tableCell7.Weight = 0.078725092017416065D;
            // 
            // tableCell8
            // 
            this.tableCell8.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[id_territoire]")});
            this.tableCell8.Name = "tableCell8";
            this.tableCell8.StyleName = "GroupData1";
            this.tableCell8.Weight = 0.92127488187687123D;
            // 
            // table5
            // 
            this.table5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.table5.Name = "table5";
            this.table5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow5});
            this.table5.SizeF = new System.Drawing.SizeF(1169F, 28F);
            // 
            // tableRow5
            // 
            this.tableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell9,
            this.tableCell10,
            this.tableCell11,
            this.tableCell12,
            this.tableCell13,
            this.tableCell14,
            this.tableCell15,
            this.tableCell16,
            this.tableCell17,
            this.tableCell18,
            this.tableCell19,
            this.tableCell20,
            this.tableCell21,
            this.tableCell22,
            this.tableCell23,
            this.tableCell24,
            this.tableCell25,
            this.tableCell26});
            this.tableRow5.Name = "tableRow5";
            this.tableRow5.Weight = 1D;
            // 
            // tableCell9
            // 
            this.tableCell9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.tableCell9.Name = "tableCell9";
            this.tableCell9.StyleName = "DetailCaption1";
            this.tableCell9.StylePriority.UseBorders = false;
            this.tableCell9.Text = "num sensibilisation";
            this.tableCell9.Weight = 0.06116138389724457D;
            // 
            // tableCell10
            // 
            this.tableCell10.Name = "tableCell10";
            this.tableCell10.StyleName = "DetailCaption1";
            this.tableCell10.Text = "date debut";
            this.tableCell10.Weight = 0.036386070545154109D;
            // 
            // tableCell11
            // 
            this.tableCell11.Name = "tableCell11";
            this.tableCell11.StyleName = "DetailCaption1";
            this.tableCell11.Text = "date fin";
            this.tableCell11.Weight = 0.027701705005864389D;
            // 
            // tableCell12
            // 
            this.tableCell12.Name = "tableCell12";
            this.tableCell12.StyleName = "DetailCaption1";
            this.tableCell12.Text = "id province";
            this.tableCell12.Weight = 0.038000252024550228D;
            // 
            // tableCell13
            // 
            this.tableCell13.Name = "tableCell13";
            this.tableCell13.StyleName = "DetailCaption1";
            this.tableCell13.StylePriority.UseTextAlignment = false;
            this.tableCell13.Text = "num atelier masse";
            this.tableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell13.Weight = 0.057639018402230138D;
            // 
            // tableCell14
            // 
            this.tableCell14.Name = "tableCell14";
            this.tableCell14.StyleName = "DetailCaption1";
            this.tableCell14.Text = "noms sensibilisateur";
            this.tableCell14.Weight = 0.064709195935695574D;
            // 
            // tableCell15
            // 
            this.tableCell15.Name = "tableCell15";
            this.tableCell15.StyleName = "DetailCaption1";
            this.tableCell15.StylePriority.UseTextAlignment = false;
            this.tableCell15.Text = "nbre hommes";
            this.tableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell15.Weight = 0.045078917177851327D;
            // 
            // tableCell16
            // 
            this.tableCell16.Name = "tableCell16";
            this.tableCell16.StyleName = "DetailCaption1";
            this.tableCell16.StylePriority.UseTextAlignment = false;
            this.tableCell16.Text = "nbre femmes";
            this.tableCell16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell16.Weight = 0.043153770975418025D;
            // 
            // tableCell17
            // 
            this.tableCell17.Name = "tableCell17";
            this.tableCell17.StyleName = "DetailCaption1";
            this.tableCell17.StylePriority.UseTextAlignment = false;
            this.tableCell17.Text = "nbre filles";
            this.tableCell17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell17.Weight = 0.034463747022904528D;
            // 
            // tableCell18
            // 
            this.tableCell18.Name = "tableCell18";
            this.tableCell18.StyleName = "DetailCaption1";
            this.tableCell18.StylePriority.UseTextAlignment = false;
            this.tableCell18.Text = "nbre garcons";
            this.tableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell18.Weight = 0.043792656081879211D;
            // 
            // tableCell19
            // 
            this.tableCell19.Name = "tableCell19";
            this.tableCell19.StyleName = "DetailCaption1";
            this.tableCell19.StylePriority.UseTextAlignment = false;
            this.tableCell19.Text = "nbre autorite femmes";
            this.tableCell19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell19.Weight = 0.065992624561841712D;
            // 
            // tableCell20
            // 
            this.tableCell20.Name = "tableCell20";
            this.tableCell20.StyleName = "DetailCaption1";
            this.tableCell20.StylePriority.UseTextAlignment = false;
            this.tableCell20.Text = "nbre autorite hommes";
            this.tableCell20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell20.Weight = 0.067917770764275021D;
            // 
            // tableCell21
            // 
            this.tableCell21.Name = "tableCell21";
            this.tableCell21.StyleName = "DetailCaption1";
            this.tableCell21.StylePriority.UseTextAlignment = false;
            this.tableCell21.Text = "nbre menages deplaces";
            this.tableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell21.Weight = 0.072760321728891547D;
            // 
            // tableCell22
            // 
            this.tableCell22.Name = "tableCell22";
            this.tableCell22.StyleName = "DetailCaption1";
            this.tableCell22.StylePriority.UseTextAlignment = false;
            this.tableCell22.Text = "nbre menages retournes";
            this.tableCell22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell22.Weight = 0.074682648514355215D;
            // 
            // tableCell23
            // 
            this.tableCell23.Name = "tableCell23";
            this.tableCell23.StyleName = "DetailCaption1";
            this.tableCell23.StylePriority.UseTextAlignment = false;
            this.tableCell23.Text = "nbre menages locaux";
            this.tableCell23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell23.Weight = 0.066320551471693884D;
            // 
            // tableCell24
            // 
            this.tableCell24.Name = "tableCell24";
            this.tableCell24.StyleName = "DetailCaption1";
            this.tableCell24.StylePriority.UseTextAlignment = false;
            this.tableCell24.Text = "nbre menages rapatrie";
            this.tableCell24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell24.Weight = 0.069218168103664324D;
            // 
            // tableCell25
            // 
            this.tableCell25.Name = "tableCell25";
            this.tableCell25.StyleName = "DetailCaption1";
            this.tableCell25.Text = "telephone sensibilisateur";
            this.tableCell25.Weight = 0.076610614133758151D;
            // 
            // tableCell26
            // 
            this.tableCell26.Name = "tableCell26";
            this.tableCell26.StyleName = "DetailCaption1";
            this.tableCell26.Text = "theme developpe";
            this.tableCell26.Weight = 0.054410544494159004D;
            // 
            // table6
            // 
            this.table6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.table6.Name = "table6";
            this.table6.OddStyleName = "DetailData3_Odd";
            this.table6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.tableRow6});
            this.table6.SizeF = new System.Drawing.SizeF(1169F, 25F);
            // 
            // tableRow6
            // 
            this.tableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.tableCell27,
            this.tableCell28,
            this.tableCell29,
            this.tableCell30,
            this.tableCell31,
            this.tableCell32,
            this.tableCell33,
            this.tableCell34,
            this.tableCell35,
            this.tableCell36,
            this.tableCell37,
            this.tableCell38,
            this.tableCell39,
            this.tableCell40,
            this.tableCell41,
            this.tableCell42,
            this.tableCell43,
            this.tableCell44});
            this.tableRow6.Name = "tableRow6";
            this.tableRow6.Weight = 11.5D;
            // 
            // tableCell27
            // 
            this.tableCell27.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.tableCell27.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[num_sensibilisation]")});
            this.tableCell27.Name = "tableCell27";
            this.tableCell27.StyleName = "DetailData1";
            this.tableCell27.StylePriority.UseBorders = false;
            this.tableCell27.Weight = 0.06116138389724457D;
            // 
            // tableCell28
            // 
            this.tableCell28.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[date_debut]")});
            this.tableCell28.Name = "tableCell28";
            this.tableCell28.StyleName = "DetailData1";
            this.tableCell28.Weight = 0.036386070545154109D;
            // 
            // tableCell29
            // 
            this.tableCell29.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[date_fin]")});
            this.tableCell29.Name = "tableCell29";
            this.tableCell29.StyleName = "DetailData1";
            this.tableCell29.Weight = 0.027701705005864389D;
            // 
            // tableCell30
            // 
            this.tableCell30.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[id_province]")});
            this.tableCell30.Name = "tableCell30";
            this.tableCell30.StyleName = "DetailData1";
            this.tableCell30.Weight = 0.038000252024550228D;
            // 
            // tableCell31
            // 
            this.tableCell31.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[num_atelier_masse]")});
            this.tableCell31.Name = "tableCell31";
            this.tableCell31.StyleName = "DetailData1";
            this.tableCell31.StylePriority.UseTextAlignment = false;
            this.tableCell31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell31.Weight = 0.057639018402230138D;
            // 
            // tableCell32
            // 
            this.tableCell32.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[noms_sensibilisateur]")});
            this.tableCell32.Name = "tableCell32";
            this.tableCell32.StyleName = "DetailData1";
            this.tableCell32.Weight = 0.064709195935695574D;
            // 
            // tableCell33
            // 
            this.tableCell33.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_hommes]")});
            this.tableCell33.Name = "tableCell33";
            this.tableCell33.StyleName = "DetailData1";
            this.tableCell33.StylePriority.UseTextAlignment = false;
            this.tableCell33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell33.Weight = 0.045078920441065415D;
            // 
            // tableCell34
            // 
            this.tableCell34.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_femmes]")});
            this.tableCell34.Name = "tableCell34";
            this.tableCell34.StyleName = "DetailData1";
            this.tableCell34.StylePriority.UseTextAlignment = false;
            this.tableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell34.Weight = 0.043153774238632113D;
            // 
            // tableCell35
            // 
            this.tableCell35.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_filles]")});
            this.tableCell35.Name = "tableCell35";
            this.tableCell35.StyleName = "DetailData1";
            this.tableCell35.StylePriority.UseTextAlignment = false;
            this.tableCell35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell35.Weight = 0.034463747022904528D;
            // 
            // tableCell36
            // 
            this.tableCell36.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_garcons]")});
            this.tableCell36.Name = "tableCell36";
            this.tableCell36.StyleName = "DetailData1";
            this.tableCell36.StylePriority.UseTextAlignment = false;
            this.tableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell36.Weight = 0.0437926593450933D;
            // 
            // tableCell37
            // 
            this.tableCell37.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_autorite_femmes]")});
            this.tableCell37.Name = "tableCell37";
            this.tableCell37.StyleName = "DetailData1";
            this.tableCell37.StylePriority.UseTextAlignment = false;
            this.tableCell37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell37.Weight = 0.065992624561841712D;
            // 
            // tableCell38
            // 
            this.tableCell38.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_autorite_hommes]")});
            this.tableCell38.Name = "tableCell38";
            this.tableCell38.StyleName = "DetailData1";
            this.tableCell38.StylePriority.UseTextAlignment = false;
            this.tableCell38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell38.Weight = 0.067917770764275021D;
            // 
            // tableCell39
            // 
            this.tableCell39.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_menages_deplaces]")});
            this.tableCell39.Name = "tableCell39";
            this.tableCell39.StyleName = "DetailData1";
            this.tableCell39.StylePriority.UseTextAlignment = false;
            this.tableCell39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell39.Weight = 0.072760321728891547D;
            // 
            // tableCell40
            // 
            this.tableCell40.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_menages_retournes]")});
            this.tableCell40.Name = "tableCell40";
            this.tableCell40.StyleName = "DetailData1";
            this.tableCell40.StylePriority.UseTextAlignment = false;
            this.tableCell40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell40.Weight = 0.074682648514355215D;
            // 
            // tableCell41
            // 
            this.tableCell41.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_menages_locaux]")});
            this.tableCell41.Name = "tableCell41";
            this.tableCell41.StyleName = "DetailData1";
            this.tableCell41.StylePriority.UseTextAlignment = false;
            this.tableCell41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell41.Weight = 0.066320551471693884D;
            // 
            // tableCell42
            // 
            this.tableCell42.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[nbre_menages_rapatrie]")});
            this.tableCell42.Name = "tableCell42";
            this.tableCell42.StyleName = "DetailData1";
            this.tableCell42.StylePriority.UseTextAlignment = false;
            this.tableCell42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.tableCell42.Weight = 0.069218168103664324D;
            // 
            // tableCell43
            // 
            this.tableCell43.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[telephone_sensibilisateur]")});
            this.tableCell43.Name = "tableCell43";
            this.tableCell43.StyleName = "DetailData1";
            this.tableCell43.Weight = 0.076610614133758151D;
            // 
            // tableCell44
            // 
            this.tableCell44.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "[theme_developpe]")});
            this.tableCell44.Name = "tableCell44";
            this.tableCell44.StyleName = "DetailData1";
            this.tableCell44.Weight = 0.054410570599871685D;
            // 
            // label2
            // 
            this.label2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.label2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.label2.Name = "label2";
            this.label2.SizeF = new System.Drawing.SizeF(1169F, 2.08F);
            this.label2.StyleName = "GroupFooterBackground3";
            this.label2.StylePriority.UseBorders = false;
            // 
            // panel1
            // 
            this.panel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label3,
            this.label4,
            this.label5,
            this.label6,
            this.label7,
            this.label8,
            this.label9,
            this.label10,
            this.label11,
            this.label12,
            this.label13,
            this.label14,
            this.label15,
            this.label16,
            this.label17,
            this.label18,
            this.label19,
            this.label20,
            this.label21,
            this.label22,
            this.label23});
            this.panel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.panel1.Name = "panel1";
            this.panel1.SizeF = new System.Drawing.SizeF(1169F, 56.16888F);
            this.panel1.StyleName = "TotalBackground1";
            // 
            // label3
            // 
            this.label3.LocationFloat = new DevExpress.Utils.PointFloat(333.8636F, 3.4F);
            this.label3.Name = "label3";
            this.label3.SizeF = new System.Drawing.SizeF(26.5F, 14.88444F);
            this.label3.StyleName = "TotalCaption1";
            this.label3.Text = "SUM";
            // 
            // label4
            // 
            this.label4.CanGrow = false;
            this.label4.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_hommes])")});
            this.label4.LocationFloat = new DevExpress.Utils.PointFloat(360.3636F, 3.4F);
            this.label4.Name = "label4";
            this.label4.SizeF = new System.Drawing.SizeF(26.19727F, 14.88444F);
            this.label4.StyleName = "TotalData1";
            this.label4.StylePriority.UseTextAlignment = false;
            xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label4.Summary = xrSummary1;
            this.label4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label4.WordWrap = false;
            // 
            // label5
            // 
            this.label5.LocationFloat = new DevExpress.Utils.PointFloat(386.5609F, 3.4F);
            this.label5.Name = "label5";
            this.label5.SizeF = new System.Drawing.SizeF(25F, 14.88444F);
            this.label5.StyleName = "TotalCaption1";
            this.label5.Text = "SUM";
            // 
            // label6
            // 
            this.label6.CanGrow = false;
            this.label6.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_femmes])")});
            this.label6.LocationFloat = new DevExpress.Utils.PointFloat(411.5609F, 3.4F);
            this.label6.Name = "label6";
            this.label6.SizeF = new System.Drawing.SizeF(25.44675F, 14.88444F);
            this.label6.StyleName = "TotalData1";
            this.label6.StylePriority.UseTextAlignment = false;
            xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label6.Summary = xrSummary2;
            this.label6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label6.WordWrap = false;
            // 
            // label7
            // 
            this.label7.LocationFloat = new DevExpress.Utils.PointFloat(437.0076F, 3.4F);
            this.label7.Name = "label7";
            this.label7.SizeF = new System.Drawing.SizeF(20F, 14.88444F);
            this.label7.StyleName = "TotalCaption1";
            this.label7.Text = "SUM";
            // 
            // label8
            // 
            this.label8.CanGrow = false;
            this.label8.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_filles])")});
            this.label8.LocationFloat = new DevExpress.Utils.PointFloat(457.0076F, 3.4F);
            this.label8.Name = "label8";
            this.label8.SizeF = new System.Drawing.SizeF(20.28812F, 14.88444F);
            this.label8.StyleName = "TotalData1";
            this.label8.StylePriority.UseTextAlignment = false;
            xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label8.Summary = xrSummary3;
            this.label8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label8.WordWrap = false;
            // 
            // label9
            // 
            this.label9.LocationFloat = new DevExpress.Utils.PointFloat(477.2957F, 3.4F);
            this.label9.Name = "label9";
            this.label9.SizeF = new System.Drawing.SizeF(25.5F, 14.88444F);
            this.label9.StyleName = "TotalCaption1";
            this.label9.Text = "SUM";
            // 
            // label10
            // 
            this.label10.CanGrow = false;
            this.label10.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_garcons])")});
            this.label10.LocationFloat = new DevExpress.Utils.PointFloat(502.7957F, 3.4F);
            this.label10.Name = "label10";
            this.label10.SizeF = new System.Drawing.SizeF(25.69363F, 14.88444F);
            this.label10.StyleName = "TotalData1";
            this.label10.StylePriority.UseTextAlignment = false;
            xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label10.Summary = xrSummary4;
            this.label10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label10.WordWrap = false;
            // 
            // label11
            // 
            this.label11.LocationFloat = new DevExpress.Utils.PointFloat(528.4894F, 3.4F);
            this.label11.Name = "label11";
            this.label11.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label11.StyleName = "TotalCaption1";
            this.label11.Text = "SUM";
            // 
            // label12
            // 
            this.label12.CanGrow = false;
            this.label12.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_femmes])")});
            this.label12.LocationFloat = new DevExpress.Utils.PointFloat(561.9517F, 3.4F);
            this.label12.Name = "label12";
            this.label12.SizeF = new System.Drawing.SizeF(43.68305F, 14.88444F);
            this.label12.StyleName = "TotalData1";
            this.label12.StylePriority.UseTextAlignment = false;
            xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label12.Summary = xrSummary5;
            this.label12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label12.WordWrap = false;
            // 
            // label13
            // 
            this.label13.LocationFloat = new DevExpress.Utils.PointFloat(605.6348F, 3.4F);
            this.label13.Name = "label13";
            this.label13.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label13.StyleName = "TotalCaption1";
            this.label13.Text = "SUM";
            // 
            // label14
            // 
            this.label14.CanGrow = false;
            this.label14.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_hommes])")});
            this.label14.LocationFloat = new DevExpress.Utils.PointFloat(639.0971F, 3.4F);
            this.label14.Name = "label14";
            this.label14.SizeF = new System.Drawing.SizeF(45.93354F, 14.88444F);
            this.label14.StyleName = "TotalData1";
            this.label14.StylePriority.UseTextAlignment = false;
            xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label14.Summary = xrSummary6;
            this.label14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label14.WordWrap = false;
            // 
            // label15
            // 
            this.label15.LocationFloat = new DevExpress.Utils.PointFloat(685.0306F, 3.4F);
            this.label15.Name = "label15";
            this.label15.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label15.StyleName = "TotalCaption1";
            this.label15.Text = "SUM";
            // 
            // label16
            // 
            this.label16.CanGrow = false;
            this.label16.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_deplaces])")});
            this.label16.LocationFloat = new DevExpress.Utils.PointFloat(718.493F, 3.4F);
            this.label16.Name = "label16";
            this.label16.SizeF = new System.Drawing.SizeF(51.59449F, 14.88444F);
            this.label16.StyleName = "TotalData1";
            this.label16.StylePriority.UseTextAlignment = false;
            xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label16.Summary = xrSummary7;
            this.label16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label16.WordWrap = false;
            // 
            // label17
            // 
            this.label17.LocationFloat = new DevExpress.Utils.PointFloat(770.0875F, 3.4F);
            this.label17.Name = "label17";
            this.label17.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label17.StyleName = "TotalCaption1";
            this.label17.Text = "SUM";
            // 
            // label18
            // 
            this.label18.CanGrow = false;
            this.label18.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_retournes])")});
            this.label18.LocationFloat = new DevExpress.Utils.PointFloat(803.5498F, 3.4F);
            this.label18.Name = "label18";
            this.label18.SizeF = new System.Drawing.SizeF(53.84168F, 14.88444F);
            this.label18.StyleName = "TotalData1";
            this.label18.StylePriority.UseTextAlignment = false;
            xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label18.Summary = xrSummary8;
            this.label18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label18.WordWrap = false;
            // 
            // label19
            // 
            this.label19.LocationFloat = new DevExpress.Utils.PointFloat(857.3915F, 3.4F);
            this.label19.Name = "label19";
            this.label19.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label19.StyleName = "TotalCaption1";
            this.label19.Text = "SUM";
            // 
            // label20
            // 
            this.label20.CanGrow = false;
            this.label20.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_locaux])")});
            this.label20.LocationFloat = new DevExpress.Utils.PointFloat(890.8538F, 3.4F);
            this.label20.Name = "label20";
            this.label20.SizeF = new System.Drawing.SizeF(44.06641F, 14.88444F);
            this.label20.StyleName = "TotalData1";
            this.label20.StylePriority.UseTextAlignment = false;
            xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label20.Summary = xrSummary9;
            this.label20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label20.WordWrap = false;
            // 
            // label21
            // 
            this.label21.LocationFloat = new DevExpress.Utils.PointFloat(934.9202F, 3.4F);
            this.label21.Name = "label21";
            this.label21.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label21.StyleName = "TotalCaption1";
            this.label21.Text = "SUM";
            // 
            // label22
            // 
            this.label22.CanGrow = false;
            this.label22.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_rapatrie])")});
            this.label22.LocationFloat = new DevExpress.Utils.PointFloat(968.3826F, 3.4F);
            this.label22.Name = "label22";
            this.label22.SizeF = new System.Drawing.SizeF(47.45368F, 14.88444F);
            this.label22.StyleName = "TotalData1";
            this.label22.StylePriority.UseTextAlignment = false;
            xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label22.Summary = xrSummary10;
            this.label22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label22.WordWrap = false;
            // 
            // label23
            // 
            this.label23.LocationFloat = new DevExpress.Utils.PointFloat(6F, 29.78444F);
            this.label23.Name = "label23";
            this.label23.SizeF = new System.Drawing.SizeF(1157F, 14.88444F);
            this.label23.StyleName = "TotalCaption1";
            this.label23.StylePriority.UseTextAlignment = false;
            this.label23.Text = "id_localite [id_localite]";
            this.label23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // panel2
            // 
            this.panel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label24,
            this.label25,
            this.label26,
            this.label27,
            this.label28,
            this.label29,
            this.label30,
            this.label31,
            this.label32,
            this.label33,
            this.label34,
            this.label35,
            this.label36,
            this.label37,
            this.label38,
            this.label39,
            this.label40,
            this.label41,
            this.label42,
            this.label43,
            this.label44});
            this.panel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.panel2.Name = "panel2";
            this.panel2.SizeF = new System.Drawing.SizeF(1169F, 56.16888F);
            this.panel2.StyleName = "TotalBackground1";
            // 
            // label24
            // 
            this.label24.LocationFloat = new DevExpress.Utils.PointFloat(333.8636F, 3.4F);
            this.label24.Name = "label24";
            this.label24.SizeF = new System.Drawing.SizeF(26.5F, 14.88444F);
            this.label24.StyleName = "TotalCaption1";
            this.label24.Text = "SUM";
            // 
            // label25
            // 
            this.label25.CanGrow = false;
            this.label25.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_hommes])")});
            this.label25.LocationFloat = new DevExpress.Utils.PointFloat(360.3636F, 3.4F);
            this.label25.Name = "label25";
            this.label25.SizeF = new System.Drawing.SizeF(26.19727F, 14.88444F);
            this.label25.StyleName = "TotalData1";
            this.label25.StylePriority.UseTextAlignment = false;
            xrSummary11.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label25.Summary = xrSummary11;
            this.label25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label25.WordWrap = false;
            // 
            // label26
            // 
            this.label26.LocationFloat = new DevExpress.Utils.PointFloat(386.5609F, 3.4F);
            this.label26.Name = "label26";
            this.label26.SizeF = new System.Drawing.SizeF(25F, 14.88444F);
            this.label26.StyleName = "TotalCaption1";
            this.label26.Text = "SUM";
            // 
            // label27
            // 
            this.label27.CanGrow = false;
            this.label27.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_femmes])")});
            this.label27.LocationFloat = new DevExpress.Utils.PointFloat(411.5609F, 3.4F);
            this.label27.Name = "label27";
            this.label27.SizeF = new System.Drawing.SizeF(25.44675F, 14.88444F);
            this.label27.StyleName = "TotalData1";
            this.label27.StylePriority.UseTextAlignment = false;
            xrSummary12.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label27.Summary = xrSummary12;
            this.label27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label27.WordWrap = false;
            // 
            // label28
            // 
            this.label28.LocationFloat = new DevExpress.Utils.PointFloat(437.0076F, 3.4F);
            this.label28.Name = "label28";
            this.label28.SizeF = new System.Drawing.SizeF(20F, 14.88444F);
            this.label28.StyleName = "TotalCaption1";
            this.label28.Text = "SUM";
            // 
            // label29
            // 
            this.label29.CanGrow = false;
            this.label29.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_filles])")});
            this.label29.LocationFloat = new DevExpress.Utils.PointFloat(457.0076F, 3.4F);
            this.label29.Name = "label29";
            this.label29.SizeF = new System.Drawing.SizeF(20.28812F, 14.88444F);
            this.label29.StyleName = "TotalData1";
            this.label29.StylePriority.UseTextAlignment = false;
            xrSummary13.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label29.Summary = xrSummary13;
            this.label29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label29.WordWrap = false;
            // 
            // label30
            // 
            this.label30.LocationFloat = new DevExpress.Utils.PointFloat(477.2957F, 3.4F);
            this.label30.Name = "label30";
            this.label30.SizeF = new System.Drawing.SizeF(25.5F, 14.88444F);
            this.label30.StyleName = "TotalCaption1";
            this.label30.Text = "SUM";
            // 
            // label31
            // 
            this.label31.CanGrow = false;
            this.label31.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_garcons])")});
            this.label31.LocationFloat = new DevExpress.Utils.PointFloat(502.7957F, 3.4F);
            this.label31.Name = "label31";
            this.label31.SizeF = new System.Drawing.SizeF(25.69363F, 14.88444F);
            this.label31.StyleName = "TotalData1";
            this.label31.StylePriority.UseTextAlignment = false;
            xrSummary14.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label31.Summary = xrSummary14;
            this.label31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label31.WordWrap = false;
            // 
            // label32
            // 
            this.label32.LocationFloat = new DevExpress.Utils.PointFloat(528.4894F, 3.4F);
            this.label32.Name = "label32";
            this.label32.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label32.StyleName = "TotalCaption1";
            this.label32.Text = "SUM";
            // 
            // label33
            // 
            this.label33.CanGrow = false;
            this.label33.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_femmes])")});
            this.label33.LocationFloat = new DevExpress.Utils.PointFloat(561.9517F, 3.4F);
            this.label33.Name = "label33";
            this.label33.SizeF = new System.Drawing.SizeF(43.68305F, 14.88444F);
            this.label33.StyleName = "TotalData1";
            this.label33.StylePriority.UseTextAlignment = false;
            xrSummary15.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label33.Summary = xrSummary15;
            this.label33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label33.WordWrap = false;
            // 
            // label34
            // 
            this.label34.LocationFloat = new DevExpress.Utils.PointFloat(605.6348F, 3.4F);
            this.label34.Name = "label34";
            this.label34.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label34.StyleName = "TotalCaption1";
            this.label34.Text = "SUM";
            // 
            // label35
            // 
            this.label35.CanGrow = false;
            this.label35.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_hommes])")});
            this.label35.LocationFloat = new DevExpress.Utils.PointFloat(639.0971F, 3.4F);
            this.label35.Name = "label35";
            this.label35.SizeF = new System.Drawing.SizeF(45.93354F, 14.88444F);
            this.label35.StyleName = "TotalData1";
            this.label35.StylePriority.UseTextAlignment = false;
            xrSummary16.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label35.Summary = xrSummary16;
            this.label35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label35.WordWrap = false;
            // 
            // label36
            // 
            this.label36.LocationFloat = new DevExpress.Utils.PointFloat(685.0306F, 3.4F);
            this.label36.Name = "label36";
            this.label36.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label36.StyleName = "TotalCaption1";
            this.label36.Text = "SUM";
            // 
            // label37
            // 
            this.label37.CanGrow = false;
            this.label37.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_deplaces])")});
            this.label37.LocationFloat = new DevExpress.Utils.PointFloat(718.493F, 3.4F);
            this.label37.Name = "label37";
            this.label37.SizeF = new System.Drawing.SizeF(51.59449F, 14.88444F);
            this.label37.StyleName = "TotalData1";
            this.label37.StylePriority.UseTextAlignment = false;
            xrSummary17.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label37.Summary = xrSummary17;
            this.label37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label37.WordWrap = false;
            // 
            // label38
            // 
            this.label38.LocationFloat = new DevExpress.Utils.PointFloat(770.0875F, 3.4F);
            this.label38.Name = "label38";
            this.label38.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label38.StyleName = "TotalCaption1";
            this.label38.Text = "SUM";
            // 
            // label39
            // 
            this.label39.CanGrow = false;
            this.label39.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_retournes])")});
            this.label39.LocationFloat = new DevExpress.Utils.PointFloat(803.5498F, 3.4F);
            this.label39.Name = "label39";
            this.label39.SizeF = new System.Drawing.SizeF(53.84168F, 14.88444F);
            this.label39.StyleName = "TotalData1";
            this.label39.StylePriority.UseTextAlignment = false;
            xrSummary18.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label39.Summary = xrSummary18;
            this.label39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label39.WordWrap = false;
            // 
            // label40
            // 
            this.label40.LocationFloat = new DevExpress.Utils.PointFloat(857.3915F, 3.4F);
            this.label40.Name = "label40";
            this.label40.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label40.StyleName = "TotalCaption1";
            this.label40.Text = "SUM";
            // 
            // label41
            // 
            this.label41.CanGrow = false;
            this.label41.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_locaux])")});
            this.label41.LocationFloat = new DevExpress.Utils.PointFloat(890.8538F, 3.4F);
            this.label41.Name = "label41";
            this.label41.SizeF = new System.Drawing.SizeF(44.06641F, 14.88444F);
            this.label41.StyleName = "TotalData1";
            this.label41.StylePriority.UseTextAlignment = false;
            xrSummary19.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label41.Summary = xrSummary19;
            this.label41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label41.WordWrap = false;
            // 
            // label42
            // 
            this.label42.LocationFloat = new DevExpress.Utils.PointFloat(934.9202F, 3.4F);
            this.label42.Name = "label42";
            this.label42.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label42.StyleName = "TotalCaption1";
            this.label42.Text = "SUM";
            // 
            // label43
            // 
            this.label43.CanGrow = false;
            this.label43.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_rapatrie])")});
            this.label43.LocationFloat = new DevExpress.Utils.PointFloat(968.3826F, 3.4F);
            this.label43.Name = "label43";
            this.label43.SizeF = new System.Drawing.SizeF(47.45368F, 14.88444F);
            this.label43.StyleName = "TotalData1";
            this.label43.StylePriority.UseTextAlignment = false;
            xrSummary20.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label43.Summary = xrSummary20;
            this.label43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label43.WordWrap = false;
            // 
            // label44
            // 
            this.label44.LocationFloat = new DevExpress.Utils.PointFloat(6F, 29.78444F);
            this.label44.Name = "label44";
            this.label44.SizeF = new System.Drawing.SizeF(1157F, 14.88444F);
            this.label44.StyleName = "TotalCaption1";
            this.label44.StylePriority.UseTextAlignment = false;
            this.label44.Text = "id_groupement [id_groupement]";
            this.label44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // panel3
            // 
            this.panel3.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label45,
            this.label46,
            this.label47,
            this.label48,
            this.label49,
            this.label50,
            this.label51,
            this.label52,
            this.label53,
            this.label54,
            this.label55,
            this.label56,
            this.label57,
            this.label58,
            this.label59,
            this.label60,
            this.label61,
            this.label62,
            this.label63,
            this.label64,
            this.label65});
            this.panel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.panel3.Name = "panel3";
            this.panel3.SizeF = new System.Drawing.SizeF(1169F, 56.16888F);
            this.panel3.StyleName = "TotalBackground1";
            // 
            // label45
            // 
            this.label45.LocationFloat = new DevExpress.Utils.PointFloat(333.8636F, 3.4F);
            this.label45.Name = "label45";
            this.label45.SizeF = new System.Drawing.SizeF(26.5F, 14.88444F);
            this.label45.StyleName = "TotalCaption1";
            this.label45.Text = "SUM";
            // 
            // label46
            // 
            this.label46.CanGrow = false;
            this.label46.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_hommes])")});
            this.label46.LocationFloat = new DevExpress.Utils.PointFloat(360.3636F, 3.4F);
            this.label46.Name = "label46";
            this.label46.SizeF = new System.Drawing.SizeF(26.19727F, 14.88444F);
            this.label46.StyleName = "TotalData1";
            this.label46.StylePriority.UseTextAlignment = false;
            xrSummary21.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label46.Summary = xrSummary21;
            this.label46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label46.WordWrap = false;
            // 
            // label47
            // 
            this.label47.LocationFloat = new DevExpress.Utils.PointFloat(386.5609F, 3.4F);
            this.label47.Name = "label47";
            this.label47.SizeF = new System.Drawing.SizeF(25F, 14.88444F);
            this.label47.StyleName = "TotalCaption1";
            this.label47.Text = "SUM";
            // 
            // label48
            // 
            this.label48.CanGrow = false;
            this.label48.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_femmes])")});
            this.label48.LocationFloat = new DevExpress.Utils.PointFloat(411.5609F, 3.4F);
            this.label48.Name = "label48";
            this.label48.SizeF = new System.Drawing.SizeF(25.44675F, 14.88444F);
            this.label48.StyleName = "TotalData1";
            this.label48.StylePriority.UseTextAlignment = false;
            xrSummary22.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label48.Summary = xrSummary22;
            this.label48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label48.WordWrap = false;
            // 
            // label49
            // 
            this.label49.LocationFloat = new DevExpress.Utils.PointFloat(437.0076F, 3.4F);
            this.label49.Name = "label49";
            this.label49.SizeF = new System.Drawing.SizeF(20F, 14.88444F);
            this.label49.StyleName = "TotalCaption1";
            this.label49.Text = "SUM";
            // 
            // label50
            // 
            this.label50.CanGrow = false;
            this.label50.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_filles])")});
            this.label50.LocationFloat = new DevExpress.Utils.PointFloat(457.0076F, 3.4F);
            this.label50.Name = "label50";
            this.label50.SizeF = new System.Drawing.SizeF(20.28812F, 14.88444F);
            this.label50.StyleName = "TotalData1";
            this.label50.StylePriority.UseTextAlignment = false;
            xrSummary23.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label50.Summary = xrSummary23;
            this.label50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label50.WordWrap = false;
            // 
            // label51
            // 
            this.label51.LocationFloat = new DevExpress.Utils.PointFloat(477.2957F, 3.4F);
            this.label51.Name = "label51";
            this.label51.SizeF = new System.Drawing.SizeF(25.5F, 14.88444F);
            this.label51.StyleName = "TotalCaption1";
            this.label51.Text = "SUM";
            // 
            // label52
            // 
            this.label52.CanGrow = false;
            this.label52.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_garcons])")});
            this.label52.LocationFloat = new DevExpress.Utils.PointFloat(502.7957F, 3.4F);
            this.label52.Name = "label52";
            this.label52.SizeF = new System.Drawing.SizeF(25.69363F, 14.88444F);
            this.label52.StyleName = "TotalData1";
            this.label52.StylePriority.UseTextAlignment = false;
            xrSummary24.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label52.Summary = xrSummary24;
            this.label52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label52.WordWrap = false;
            // 
            // label53
            // 
            this.label53.LocationFloat = new DevExpress.Utils.PointFloat(528.4894F, 3.4F);
            this.label53.Name = "label53";
            this.label53.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label53.StyleName = "TotalCaption1";
            this.label53.Text = "SUM";
            // 
            // label54
            // 
            this.label54.CanGrow = false;
            this.label54.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_femmes])")});
            this.label54.LocationFloat = new DevExpress.Utils.PointFloat(561.9517F, 3.4F);
            this.label54.Name = "label54";
            this.label54.SizeF = new System.Drawing.SizeF(43.68305F, 14.88444F);
            this.label54.StyleName = "TotalData1";
            this.label54.StylePriority.UseTextAlignment = false;
            xrSummary25.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label54.Summary = xrSummary25;
            this.label54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label54.WordWrap = false;
            // 
            // label55
            // 
            this.label55.LocationFloat = new DevExpress.Utils.PointFloat(605.6348F, 3.4F);
            this.label55.Name = "label55";
            this.label55.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label55.StyleName = "TotalCaption1";
            this.label55.Text = "SUM";
            // 
            // label56
            // 
            this.label56.CanGrow = false;
            this.label56.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_hommes])")});
            this.label56.LocationFloat = new DevExpress.Utils.PointFloat(639.0971F, 3.4F);
            this.label56.Name = "label56";
            this.label56.SizeF = new System.Drawing.SizeF(45.93354F, 14.88444F);
            this.label56.StyleName = "TotalData1";
            this.label56.StylePriority.UseTextAlignment = false;
            xrSummary26.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label56.Summary = xrSummary26;
            this.label56.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label56.WordWrap = false;
            // 
            // label57
            // 
            this.label57.LocationFloat = new DevExpress.Utils.PointFloat(685.0306F, 3.4F);
            this.label57.Name = "label57";
            this.label57.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label57.StyleName = "TotalCaption1";
            this.label57.Text = "SUM";
            // 
            // label58
            // 
            this.label58.CanGrow = false;
            this.label58.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_deplaces])")});
            this.label58.LocationFloat = new DevExpress.Utils.PointFloat(718.493F, 3.4F);
            this.label58.Name = "label58";
            this.label58.SizeF = new System.Drawing.SizeF(51.59449F, 14.88444F);
            this.label58.StyleName = "TotalData1";
            this.label58.StylePriority.UseTextAlignment = false;
            xrSummary27.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label58.Summary = xrSummary27;
            this.label58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label58.WordWrap = false;
            // 
            // label59
            // 
            this.label59.LocationFloat = new DevExpress.Utils.PointFloat(770.0875F, 3.4F);
            this.label59.Name = "label59";
            this.label59.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label59.StyleName = "TotalCaption1";
            this.label59.Text = "SUM";
            // 
            // label60
            // 
            this.label60.CanGrow = false;
            this.label60.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_retournes])")});
            this.label60.LocationFloat = new DevExpress.Utils.PointFloat(803.5498F, 3.4F);
            this.label60.Name = "label60";
            this.label60.SizeF = new System.Drawing.SizeF(53.84168F, 14.88444F);
            this.label60.StyleName = "TotalData1";
            this.label60.StylePriority.UseTextAlignment = false;
            xrSummary28.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label60.Summary = xrSummary28;
            this.label60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label60.WordWrap = false;
            // 
            // label61
            // 
            this.label61.LocationFloat = new DevExpress.Utils.PointFloat(857.3915F, 3.4F);
            this.label61.Name = "label61";
            this.label61.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label61.StyleName = "TotalCaption1";
            this.label61.Text = "SUM";
            // 
            // label62
            // 
            this.label62.CanGrow = false;
            this.label62.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_locaux])")});
            this.label62.LocationFloat = new DevExpress.Utils.PointFloat(890.8538F, 3.4F);
            this.label62.Name = "label62";
            this.label62.SizeF = new System.Drawing.SizeF(44.06641F, 14.88444F);
            this.label62.StyleName = "TotalData1";
            this.label62.StylePriority.UseTextAlignment = false;
            xrSummary29.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label62.Summary = xrSummary29;
            this.label62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label62.WordWrap = false;
            // 
            // label63
            // 
            this.label63.LocationFloat = new DevExpress.Utils.PointFloat(934.9202F, 3.4F);
            this.label63.Name = "label63";
            this.label63.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label63.StyleName = "TotalCaption1";
            this.label63.Text = "SUM";
            // 
            // label64
            // 
            this.label64.CanGrow = false;
            this.label64.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_rapatrie])")});
            this.label64.LocationFloat = new DevExpress.Utils.PointFloat(968.3826F, 3.4F);
            this.label64.Name = "label64";
            this.label64.SizeF = new System.Drawing.SizeF(47.45368F, 14.88444F);
            this.label64.StyleName = "TotalData1";
            this.label64.StylePriority.UseTextAlignment = false;
            xrSummary30.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label64.Summary = xrSummary30;
            this.label64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label64.WordWrap = false;
            // 
            // label65
            // 
            this.label65.LocationFloat = new DevExpress.Utils.PointFloat(6F, 29.78444F);
            this.label65.Name = "label65";
            this.label65.SizeF = new System.Drawing.SizeF(1157F, 14.88444F);
            this.label65.StyleName = "TotalCaption1";
            this.label65.StylePriority.UseTextAlignment = false;
            this.label65.Text = "id_chefferie [id_chefferie]";
            this.label65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // panel4
            // 
            this.panel4.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label66,
            this.label67,
            this.label68,
            this.label69,
            this.label70,
            this.label71,
            this.label72,
            this.label73,
            this.label74,
            this.label75,
            this.label76,
            this.label77,
            this.label78,
            this.label79,
            this.label80,
            this.label81,
            this.label82,
            this.label83,
            this.label84,
            this.label85,
            this.label86});
            this.panel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.panel4.Name = "panel4";
            this.panel4.SizeF = new System.Drawing.SizeF(1169F, 56.16888F);
            this.panel4.StyleName = "TotalBackground1";
            // 
            // label66
            // 
            this.label66.LocationFloat = new DevExpress.Utils.PointFloat(333.8636F, 3.4F);
            this.label66.Name = "label66";
            this.label66.SizeF = new System.Drawing.SizeF(26.5F, 14.88444F);
            this.label66.StyleName = "TotalCaption1";
            this.label66.Text = "SUM";
            // 
            // label67
            // 
            this.label67.CanGrow = false;
            this.label67.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_hommes])")});
            this.label67.LocationFloat = new DevExpress.Utils.PointFloat(360.3636F, 3.4F);
            this.label67.Name = "label67";
            this.label67.SizeF = new System.Drawing.SizeF(26.19727F, 14.88444F);
            this.label67.StyleName = "TotalData1";
            this.label67.StylePriority.UseTextAlignment = false;
            xrSummary31.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label67.Summary = xrSummary31;
            this.label67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label67.WordWrap = false;
            // 
            // label68
            // 
            this.label68.LocationFloat = new DevExpress.Utils.PointFloat(386.5609F, 3.4F);
            this.label68.Name = "label68";
            this.label68.SizeF = new System.Drawing.SizeF(25F, 14.88444F);
            this.label68.StyleName = "TotalCaption1";
            this.label68.Text = "SUM";
            // 
            // label69
            // 
            this.label69.CanGrow = false;
            this.label69.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_femmes])")});
            this.label69.LocationFloat = new DevExpress.Utils.PointFloat(411.5609F, 3.4F);
            this.label69.Name = "label69";
            this.label69.SizeF = new System.Drawing.SizeF(25.44675F, 14.88444F);
            this.label69.StyleName = "TotalData1";
            this.label69.StylePriority.UseTextAlignment = false;
            xrSummary32.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label69.Summary = xrSummary32;
            this.label69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label69.WordWrap = false;
            // 
            // label70
            // 
            this.label70.LocationFloat = new DevExpress.Utils.PointFloat(437.0076F, 3.4F);
            this.label70.Name = "label70";
            this.label70.SizeF = new System.Drawing.SizeF(20F, 14.88444F);
            this.label70.StyleName = "TotalCaption1";
            this.label70.Text = "SUM";
            // 
            // label71
            // 
            this.label71.CanGrow = false;
            this.label71.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_filles])")});
            this.label71.LocationFloat = new DevExpress.Utils.PointFloat(457.0076F, 3.4F);
            this.label71.Name = "label71";
            this.label71.SizeF = new System.Drawing.SizeF(20.28812F, 14.88444F);
            this.label71.StyleName = "TotalData1";
            this.label71.StylePriority.UseTextAlignment = false;
            xrSummary33.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label71.Summary = xrSummary33;
            this.label71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label71.WordWrap = false;
            // 
            // label72
            // 
            this.label72.LocationFloat = new DevExpress.Utils.PointFloat(477.2957F, 3.4F);
            this.label72.Name = "label72";
            this.label72.SizeF = new System.Drawing.SizeF(25.5F, 14.88444F);
            this.label72.StyleName = "TotalCaption1";
            this.label72.Text = "SUM";
            // 
            // label73
            // 
            this.label73.CanGrow = false;
            this.label73.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_garcons])")});
            this.label73.LocationFloat = new DevExpress.Utils.PointFloat(502.7957F, 3.4F);
            this.label73.Name = "label73";
            this.label73.SizeF = new System.Drawing.SizeF(25.69363F, 14.88444F);
            this.label73.StyleName = "TotalData1";
            this.label73.StylePriority.UseTextAlignment = false;
            xrSummary34.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label73.Summary = xrSummary34;
            this.label73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label73.WordWrap = false;
            // 
            // label74
            // 
            this.label74.LocationFloat = new DevExpress.Utils.PointFloat(528.4894F, 3.4F);
            this.label74.Name = "label74";
            this.label74.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label74.StyleName = "TotalCaption1";
            this.label74.Text = "SUM";
            // 
            // label75
            // 
            this.label75.CanGrow = false;
            this.label75.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_femmes])")});
            this.label75.LocationFloat = new DevExpress.Utils.PointFloat(561.9517F, 3.4F);
            this.label75.Name = "label75";
            this.label75.SizeF = new System.Drawing.SizeF(43.68305F, 14.88444F);
            this.label75.StyleName = "TotalData1";
            this.label75.StylePriority.UseTextAlignment = false;
            xrSummary35.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label75.Summary = xrSummary35;
            this.label75.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label75.WordWrap = false;
            // 
            // label76
            // 
            this.label76.LocationFloat = new DevExpress.Utils.PointFloat(605.6348F, 3.4F);
            this.label76.Name = "label76";
            this.label76.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label76.StyleName = "TotalCaption1";
            this.label76.Text = "SUM";
            // 
            // label77
            // 
            this.label77.CanGrow = false;
            this.label77.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_hommes])")});
            this.label77.LocationFloat = new DevExpress.Utils.PointFloat(639.0971F, 3.4F);
            this.label77.Name = "label77";
            this.label77.SizeF = new System.Drawing.SizeF(45.93354F, 14.88444F);
            this.label77.StyleName = "TotalData1";
            this.label77.StylePriority.UseTextAlignment = false;
            xrSummary36.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label77.Summary = xrSummary36;
            this.label77.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label77.WordWrap = false;
            // 
            // label78
            // 
            this.label78.LocationFloat = new DevExpress.Utils.PointFloat(685.0306F, 3.4F);
            this.label78.Name = "label78";
            this.label78.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label78.StyleName = "TotalCaption1";
            this.label78.Text = "SUM";
            // 
            // label79
            // 
            this.label79.CanGrow = false;
            this.label79.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_deplaces])")});
            this.label79.LocationFloat = new DevExpress.Utils.PointFloat(718.493F, 3.4F);
            this.label79.Name = "label79";
            this.label79.SizeF = new System.Drawing.SizeF(51.59449F, 14.88444F);
            this.label79.StyleName = "TotalData1";
            this.label79.StylePriority.UseTextAlignment = false;
            xrSummary37.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label79.Summary = xrSummary37;
            this.label79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label79.WordWrap = false;
            // 
            // label80
            // 
            this.label80.LocationFloat = new DevExpress.Utils.PointFloat(770.0875F, 3.4F);
            this.label80.Name = "label80";
            this.label80.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label80.StyleName = "TotalCaption1";
            this.label80.Text = "SUM";
            // 
            // label81
            // 
            this.label81.CanGrow = false;
            this.label81.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_retournes])")});
            this.label81.LocationFloat = new DevExpress.Utils.PointFloat(803.5498F, 3.4F);
            this.label81.Name = "label81";
            this.label81.SizeF = new System.Drawing.SizeF(53.84168F, 14.88444F);
            this.label81.StyleName = "TotalData1";
            this.label81.StylePriority.UseTextAlignment = false;
            xrSummary38.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label81.Summary = xrSummary38;
            this.label81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label81.WordWrap = false;
            // 
            // label82
            // 
            this.label82.LocationFloat = new DevExpress.Utils.PointFloat(857.3915F, 3.4F);
            this.label82.Name = "label82";
            this.label82.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label82.StyleName = "TotalCaption1";
            this.label82.Text = "SUM";
            // 
            // label83
            // 
            this.label83.CanGrow = false;
            this.label83.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_locaux])")});
            this.label83.LocationFloat = new DevExpress.Utils.PointFloat(890.8538F, 3.4F);
            this.label83.Name = "label83";
            this.label83.SizeF = new System.Drawing.SizeF(44.06641F, 14.88444F);
            this.label83.StyleName = "TotalData1";
            this.label83.StylePriority.UseTextAlignment = false;
            xrSummary39.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label83.Summary = xrSummary39;
            this.label83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label83.WordWrap = false;
            // 
            // label84
            // 
            this.label84.LocationFloat = new DevExpress.Utils.PointFloat(934.9202F, 3.4F);
            this.label84.Name = "label84";
            this.label84.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label84.StyleName = "TotalCaption1";
            this.label84.Text = "SUM";
            // 
            // label85
            // 
            this.label85.CanGrow = false;
            this.label85.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_rapatrie])")});
            this.label85.LocationFloat = new DevExpress.Utils.PointFloat(968.3826F, 3.4F);
            this.label85.Name = "label85";
            this.label85.SizeF = new System.Drawing.SizeF(47.45368F, 14.88444F);
            this.label85.StyleName = "TotalData1";
            this.label85.StylePriority.UseTextAlignment = false;
            xrSummary40.Running = DevExpress.XtraReports.UI.SummaryRunning.Group;
            this.label85.Summary = xrSummary40;
            this.label85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label85.WordWrap = false;
            // 
            // label86
            // 
            this.label86.LocationFloat = new DevExpress.Utils.PointFloat(6F, 29.78444F);
            this.label86.Name = "label86";
            this.label86.SizeF = new System.Drawing.SizeF(1157F, 14.88444F);
            this.label86.StyleName = "TotalCaption1";
            this.label86.StylePriority.UseTextAlignment = false;
            this.label86.Text = "id_territoire [id_territoire]";
            this.label86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // panel5
            // 
            this.panel5.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.label87,
            this.label88,
            this.label89,
            this.label90,
            this.label91,
            this.label92,
            this.label93,
            this.label94,
            this.label95,
            this.label96,
            this.label97,
            this.label98,
            this.label99,
            this.label100,
            this.label101,
            this.label102,
            this.label103,
            this.label104,
            this.label105,
            this.label106});
            this.panel5.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.panel5.Name = "panel5";
            this.panel5.SizeF = new System.Drawing.SizeF(1169F, 49.38444F);
            this.panel5.StyleName = "GrandTotalBackground1";
            // 
            // label87
            // 
            this.label87.LocationFloat = new DevExpress.Utils.PointFloat(333.8636F, 11.5F);
            this.label87.Name = "label87";
            this.label87.SizeF = new System.Drawing.SizeF(26.5F, 14.88444F);
            this.label87.StyleName = "GrandTotalCaption1";
            this.label87.Text = "SUM";
            // 
            // label88
            // 
            this.label88.CanGrow = false;
            this.label88.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_hommes])")});
            this.label88.LocationFloat = new DevExpress.Utils.PointFloat(360.3636F, 11.5F);
            this.label88.Name = "label88";
            this.label88.SizeF = new System.Drawing.SizeF(26.19727F, 14.88444F);
            this.label88.StyleName = "GrandTotalData1";
            this.label88.StylePriority.UseTextAlignment = false;
            xrSummary41.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label88.Summary = xrSummary41;
            this.label88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label88.WordWrap = false;
            // 
            // label89
            // 
            this.label89.LocationFloat = new DevExpress.Utils.PointFloat(386.5609F, 11.5F);
            this.label89.Name = "label89";
            this.label89.SizeF = new System.Drawing.SizeF(25F, 14.88444F);
            this.label89.StyleName = "GrandTotalCaption1";
            this.label89.Text = "SUM";
            // 
            // label90
            // 
            this.label90.CanGrow = false;
            this.label90.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_femmes])")});
            this.label90.LocationFloat = new DevExpress.Utils.PointFloat(411.5609F, 11.5F);
            this.label90.Name = "label90";
            this.label90.SizeF = new System.Drawing.SizeF(25.44675F, 14.88444F);
            this.label90.StyleName = "GrandTotalData1";
            this.label90.StylePriority.UseTextAlignment = false;
            xrSummary42.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label90.Summary = xrSummary42;
            this.label90.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label90.WordWrap = false;
            // 
            // label91
            // 
            this.label91.LocationFloat = new DevExpress.Utils.PointFloat(437.0076F, 11.5F);
            this.label91.Name = "label91";
            this.label91.SizeF = new System.Drawing.SizeF(20F, 14.88444F);
            this.label91.StyleName = "GrandTotalCaption1";
            this.label91.Text = "SUM";
            // 
            // label92
            // 
            this.label92.CanGrow = false;
            this.label92.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_filles])")});
            this.label92.LocationFloat = new DevExpress.Utils.PointFloat(457.0076F, 11.5F);
            this.label92.Name = "label92";
            this.label92.SizeF = new System.Drawing.SizeF(20.28812F, 14.88444F);
            this.label92.StyleName = "GrandTotalData1";
            this.label92.StylePriority.UseTextAlignment = false;
            xrSummary43.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label92.Summary = xrSummary43;
            this.label92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label92.WordWrap = false;
            // 
            // label93
            // 
            this.label93.LocationFloat = new DevExpress.Utils.PointFloat(477.2957F, 11.5F);
            this.label93.Name = "label93";
            this.label93.SizeF = new System.Drawing.SizeF(25.5F, 14.88444F);
            this.label93.StyleName = "GrandTotalCaption1";
            this.label93.Text = "SUM";
            // 
            // label94
            // 
            this.label94.CanGrow = false;
            this.label94.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_garcons])")});
            this.label94.LocationFloat = new DevExpress.Utils.PointFloat(502.7957F, 11.5F);
            this.label94.Name = "label94";
            this.label94.SizeF = new System.Drawing.SizeF(25.69363F, 14.88444F);
            this.label94.StyleName = "GrandTotalData1";
            this.label94.StylePriority.UseTextAlignment = false;
            xrSummary44.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label94.Summary = xrSummary44;
            this.label94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label94.WordWrap = false;
            // 
            // label95
            // 
            this.label95.LocationFloat = new DevExpress.Utils.PointFloat(528.4894F, 11.5F);
            this.label95.Name = "label95";
            this.label95.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label95.StyleName = "GrandTotalCaption1";
            this.label95.Text = "SUM";
            // 
            // label96
            // 
            this.label96.CanGrow = false;
            this.label96.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_femmes])")});
            this.label96.LocationFloat = new DevExpress.Utils.PointFloat(561.9517F, 11.5F);
            this.label96.Name = "label96";
            this.label96.SizeF = new System.Drawing.SizeF(43.68305F, 14.88444F);
            this.label96.StyleName = "GrandTotalData1";
            this.label96.StylePriority.UseTextAlignment = false;
            xrSummary45.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label96.Summary = xrSummary45;
            this.label96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label96.WordWrap = false;
            // 
            // label97
            // 
            this.label97.LocationFloat = new DevExpress.Utils.PointFloat(605.6348F, 11.5F);
            this.label97.Name = "label97";
            this.label97.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label97.StyleName = "GrandTotalCaption1";
            this.label97.Text = "SUM";
            // 
            // label98
            // 
            this.label98.CanGrow = false;
            this.label98.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_autorite_hommes])")});
            this.label98.LocationFloat = new DevExpress.Utils.PointFloat(639.0971F, 11.5F);
            this.label98.Name = "label98";
            this.label98.SizeF = new System.Drawing.SizeF(45.93354F, 14.88444F);
            this.label98.StyleName = "GrandTotalData1";
            this.label98.StylePriority.UseTextAlignment = false;
            xrSummary46.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label98.Summary = xrSummary46;
            this.label98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label98.WordWrap = false;
            // 
            // label99
            // 
            this.label99.LocationFloat = new DevExpress.Utils.PointFloat(685.0306F, 11.5F);
            this.label99.Name = "label99";
            this.label99.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label99.StyleName = "GrandTotalCaption1";
            this.label99.Text = "SUM";
            // 
            // label100
            // 
            this.label100.CanGrow = false;
            this.label100.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_deplaces])")});
            this.label100.LocationFloat = new DevExpress.Utils.PointFloat(718.493F, 11.5F);
            this.label100.Name = "label100";
            this.label100.SizeF = new System.Drawing.SizeF(51.59449F, 14.88444F);
            this.label100.StyleName = "GrandTotalData1";
            this.label100.StylePriority.UseTextAlignment = false;
            xrSummary47.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label100.Summary = xrSummary47;
            this.label100.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label100.WordWrap = false;
            // 
            // label101
            // 
            this.label101.LocationFloat = new DevExpress.Utils.PointFloat(770.0875F, 11.5F);
            this.label101.Name = "label101";
            this.label101.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label101.StyleName = "GrandTotalCaption1";
            this.label101.Text = "SUM";
            // 
            // label102
            // 
            this.label102.CanGrow = false;
            this.label102.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_retournes])")});
            this.label102.LocationFloat = new DevExpress.Utils.PointFloat(803.5498F, 11.5F);
            this.label102.Name = "label102";
            this.label102.SizeF = new System.Drawing.SizeF(53.84168F, 14.88444F);
            this.label102.StyleName = "GrandTotalData1";
            this.label102.StylePriority.UseTextAlignment = false;
            xrSummary48.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label102.Summary = xrSummary48;
            this.label102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label102.WordWrap = false;
            // 
            // label103
            // 
            this.label103.LocationFloat = new DevExpress.Utils.PointFloat(857.3915F, 11.5F);
            this.label103.Name = "label103";
            this.label103.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label103.StyleName = "GrandTotalCaption1";
            this.label103.Text = "SUM";
            // 
            // label104
            // 
            this.label104.CanGrow = false;
            this.label104.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_locaux])")});
            this.label104.LocationFloat = new DevExpress.Utils.PointFloat(890.8538F, 11.5F);
            this.label104.Name = "label104";
            this.label104.SizeF = new System.Drawing.SizeF(44.06641F, 14.88444F);
            this.label104.StyleName = "GrandTotalData1";
            this.label104.StylePriority.UseTextAlignment = false;
            xrSummary49.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label104.Summary = xrSummary49;
            this.label104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label104.WordWrap = false;
            // 
            // label105
            // 
            this.label105.LocationFloat = new DevExpress.Utils.PointFloat(934.9202F, 11.5F);
            this.label105.Name = "label105";
            this.label105.SizeF = new System.Drawing.SizeF(33.46234F, 14.88444F);
            this.label105.StyleName = "GrandTotalCaption1";
            this.label105.Text = "SUM";
            // 
            // label106
            // 
            this.label106.CanGrow = false;
            this.label106.ExpressionBindings.AddRange(new DevExpress.XtraReports.UI.ExpressionBinding[] {
            new DevExpress.XtraReports.UI.ExpressionBinding("BeforePrint", "Text", "sumSum([nbre_menages_rapatrie])")});
            this.label106.LocationFloat = new DevExpress.Utils.PointFloat(968.3826F, 11.5F);
            this.label106.Name = "label106";
            this.label106.SizeF = new System.Drawing.SizeF(47.45368F, 14.88444F);
            this.label106.StyleName = "GrandTotalData1";
            this.label106.StylePriority.UseTextAlignment = false;
            xrSummary50.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
            this.label106.Summary = xrSummary50;
            this.label106.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.label106.WordWrap = false;
            // 
            // rpt_sensibilises
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupHeader1,
            this.GroupHeader2,
            this.GroupHeader3,
            this.GroupHeader4,
            this.GroupHeader5,
            this.Detail,
            this.GroupFooter1,
            this.GroupFooter2,
            this.GroupFooter3,
            this.GroupFooter4,
            this.GroupFooter5,
            this.ReportFooter});
            this.ComponentStorage.AddRange(new System.ComponentModel.IComponent[] {
            this.sqlDataSource1});
            this.DataMember = "liste_sensibilises";
            this.DataSource = this.sqlDataSource1;
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(0, 0, 100, 29);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.Title,
            this.GroupCaption1,
            this.GroupData1,
            this.DetailCaption1,
            this.DetailData1,
            this.GroupFooterBackground3,
            this.DetailData3_Odd,
            this.TotalCaption1,
            this.TotalData1,
            this.TotalBackground1,
            this.GrandTotalCaption1,
            this.GrandTotalData1,
            this.GrandTotalBackground1,
            this.PageInfo});
            this.Version = "19.1";
            ((System.ComponentModel.ISupportInitialize)(this.table1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.table6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.DataAccess.Sql.SqlDataSource sqlDataSource1;
        private DevExpress.XtraReports.UI.XRControlStyle Title;
        private DevExpress.XtraReports.UI.XRControlStyle GroupCaption1;
        private DevExpress.XtraReports.UI.XRControlStyle GroupData1;
        private DevExpress.XtraReports.UI.XRControlStyle DetailCaption1;
        private DevExpress.XtraReports.UI.XRControlStyle DetailData1;
        private DevExpress.XtraReports.UI.XRControlStyle GroupFooterBackground3;
        private DevExpress.XtraReports.UI.XRControlStyle DetailData3_Odd;
        private DevExpress.XtraReports.UI.XRControlStyle TotalCaption1;
        private DevExpress.XtraReports.UI.XRControlStyle TotalData1;
        private DevExpress.XtraReports.UI.XRControlStyle TotalBackground1;
        private DevExpress.XtraReports.UI.XRControlStyle GrandTotalCaption1;
        private DevExpress.XtraReports.UI.XRControlStyle GrandTotalData1;
        private DevExpress.XtraReports.UI.XRControlStyle GrandTotalBackground1;
        private DevExpress.XtraReports.UI.XRControlStyle PageInfo;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPageInfo pageInfo1;
        private DevExpress.XtraReports.UI.XRPageInfo pageInfo2;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel label1;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRTable table1;
        private DevExpress.XtraReports.UI.XRTableRow tableRow1;
        private DevExpress.XtraReports.UI.XRTableCell tableCell1;
        private DevExpress.XtraReports.UI.XRTableCell tableCell2;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader2;
        private DevExpress.XtraReports.UI.XRTable table2;
        private DevExpress.XtraReports.UI.XRTableRow tableRow2;
        private DevExpress.XtraReports.UI.XRTableCell tableCell3;
        private DevExpress.XtraReports.UI.XRTableCell tableCell4;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader3;
        private DevExpress.XtraReports.UI.XRTable table3;
        private DevExpress.XtraReports.UI.XRTableRow tableRow3;
        private DevExpress.XtraReports.UI.XRTableCell tableCell5;
        private DevExpress.XtraReports.UI.XRTableCell tableCell6;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader4;
        private DevExpress.XtraReports.UI.XRTable table4;
        private DevExpress.XtraReports.UI.XRTableRow tableRow4;
        private DevExpress.XtraReports.UI.XRTableCell tableCell7;
        private DevExpress.XtraReports.UI.XRTableCell tableCell8;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader5;
        private DevExpress.XtraReports.UI.XRTable table5;
        private DevExpress.XtraReports.UI.XRTableRow tableRow5;
        private DevExpress.XtraReports.UI.XRTableCell tableCell9;
        private DevExpress.XtraReports.UI.XRTableCell tableCell10;
        private DevExpress.XtraReports.UI.XRTableCell tableCell11;
        private DevExpress.XtraReports.UI.XRTableCell tableCell12;
        private DevExpress.XtraReports.UI.XRTableCell tableCell13;
        private DevExpress.XtraReports.UI.XRTableCell tableCell14;
        private DevExpress.XtraReports.UI.XRTableCell tableCell15;
        private DevExpress.XtraReports.UI.XRTableCell tableCell16;
        private DevExpress.XtraReports.UI.XRTableCell tableCell17;
        private DevExpress.XtraReports.UI.XRTableCell tableCell18;
        private DevExpress.XtraReports.UI.XRTableCell tableCell19;
        private DevExpress.XtraReports.UI.XRTableCell tableCell20;
        private DevExpress.XtraReports.UI.XRTableCell tableCell21;
        private DevExpress.XtraReports.UI.XRTableCell tableCell22;
        private DevExpress.XtraReports.UI.XRTableCell tableCell23;
        private DevExpress.XtraReports.UI.XRTableCell tableCell24;
        private DevExpress.XtraReports.UI.XRTableCell tableCell25;
        private DevExpress.XtraReports.UI.XRTableCell tableCell26;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRTable table6;
        private DevExpress.XtraReports.UI.XRTableRow tableRow6;
        private DevExpress.XtraReports.UI.XRTableCell tableCell27;
        private DevExpress.XtraReports.UI.XRTableCell tableCell28;
        private DevExpress.XtraReports.UI.XRTableCell tableCell29;
        private DevExpress.XtraReports.UI.XRTableCell tableCell30;
        private DevExpress.XtraReports.UI.XRTableCell tableCell31;
        private DevExpress.XtraReports.UI.XRTableCell tableCell32;
        private DevExpress.XtraReports.UI.XRTableCell tableCell33;
        private DevExpress.XtraReports.UI.XRTableCell tableCell34;
        private DevExpress.XtraReports.UI.XRTableCell tableCell35;
        private DevExpress.XtraReports.UI.XRTableCell tableCell36;
        private DevExpress.XtraReports.UI.XRTableCell tableCell37;
        private DevExpress.XtraReports.UI.XRTableCell tableCell38;
        private DevExpress.XtraReports.UI.XRTableCell tableCell39;
        private DevExpress.XtraReports.UI.XRTableCell tableCell40;
        private DevExpress.XtraReports.UI.XRTableCell tableCell41;
        private DevExpress.XtraReports.UI.XRTableCell tableCell42;
        private DevExpress.XtraReports.UI.XRTableCell tableCell43;
        private DevExpress.XtraReports.UI.XRTableCell tableCell44;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter1;
        private DevExpress.XtraReports.UI.XRLabel label2;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter2;
        private DevExpress.XtraReports.UI.XRPanel panel1;
        private DevExpress.XtraReports.UI.XRLabel label3;
        private DevExpress.XtraReports.UI.XRLabel label4;
        private DevExpress.XtraReports.UI.XRLabel label5;
        private DevExpress.XtraReports.UI.XRLabel label6;
        private DevExpress.XtraReports.UI.XRLabel label7;
        private DevExpress.XtraReports.UI.XRLabel label8;
        private DevExpress.XtraReports.UI.XRLabel label9;
        private DevExpress.XtraReports.UI.XRLabel label10;
        private DevExpress.XtraReports.UI.XRLabel label11;
        private DevExpress.XtraReports.UI.XRLabel label12;
        private DevExpress.XtraReports.UI.XRLabel label13;
        private DevExpress.XtraReports.UI.XRLabel label14;
        private DevExpress.XtraReports.UI.XRLabel label15;
        private DevExpress.XtraReports.UI.XRLabel label16;
        private DevExpress.XtraReports.UI.XRLabel label17;
        private DevExpress.XtraReports.UI.XRLabel label18;
        private DevExpress.XtraReports.UI.XRLabel label19;
        private DevExpress.XtraReports.UI.XRLabel label20;
        private DevExpress.XtraReports.UI.XRLabel label21;
        private DevExpress.XtraReports.UI.XRLabel label22;
        private DevExpress.XtraReports.UI.XRLabel label23;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter3;
        private DevExpress.XtraReports.UI.XRPanel panel2;
        private DevExpress.XtraReports.UI.XRLabel label24;
        private DevExpress.XtraReports.UI.XRLabel label25;
        private DevExpress.XtraReports.UI.XRLabel label26;
        private DevExpress.XtraReports.UI.XRLabel label27;
        private DevExpress.XtraReports.UI.XRLabel label28;
        private DevExpress.XtraReports.UI.XRLabel label29;
        private DevExpress.XtraReports.UI.XRLabel label30;
        private DevExpress.XtraReports.UI.XRLabel label31;
        private DevExpress.XtraReports.UI.XRLabel label32;
        private DevExpress.XtraReports.UI.XRLabel label33;
        private DevExpress.XtraReports.UI.XRLabel label34;
        private DevExpress.XtraReports.UI.XRLabel label35;
        private DevExpress.XtraReports.UI.XRLabel label36;
        private DevExpress.XtraReports.UI.XRLabel label37;
        private DevExpress.XtraReports.UI.XRLabel label38;
        private DevExpress.XtraReports.UI.XRLabel label39;
        private DevExpress.XtraReports.UI.XRLabel label40;
        private DevExpress.XtraReports.UI.XRLabel label41;
        private DevExpress.XtraReports.UI.XRLabel label42;
        private DevExpress.XtraReports.UI.XRLabel label43;
        private DevExpress.XtraReports.UI.XRLabel label44;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter4;
        private DevExpress.XtraReports.UI.XRPanel panel3;
        private DevExpress.XtraReports.UI.XRLabel label45;
        private DevExpress.XtraReports.UI.XRLabel label46;
        private DevExpress.XtraReports.UI.XRLabel label47;
        private DevExpress.XtraReports.UI.XRLabel label48;
        private DevExpress.XtraReports.UI.XRLabel label49;
        private DevExpress.XtraReports.UI.XRLabel label50;
        private DevExpress.XtraReports.UI.XRLabel label51;
        private DevExpress.XtraReports.UI.XRLabel label52;
        private DevExpress.XtraReports.UI.XRLabel label53;
        private DevExpress.XtraReports.UI.XRLabel label54;
        private DevExpress.XtraReports.UI.XRLabel label55;
        private DevExpress.XtraReports.UI.XRLabel label56;
        private DevExpress.XtraReports.UI.XRLabel label57;
        private DevExpress.XtraReports.UI.XRLabel label58;
        private DevExpress.XtraReports.UI.XRLabel label59;
        private DevExpress.XtraReports.UI.XRLabel label60;
        private DevExpress.XtraReports.UI.XRLabel label61;
        private DevExpress.XtraReports.UI.XRLabel label62;
        private DevExpress.XtraReports.UI.XRLabel label63;
        private DevExpress.XtraReports.UI.XRLabel label64;
        private DevExpress.XtraReports.UI.XRLabel label65;
        private DevExpress.XtraReports.UI.GroupFooterBand GroupFooter5;
        private DevExpress.XtraReports.UI.XRPanel panel4;
        private DevExpress.XtraReports.UI.XRLabel label66;
        private DevExpress.XtraReports.UI.XRLabel label67;
        private DevExpress.XtraReports.UI.XRLabel label68;
        private DevExpress.XtraReports.UI.XRLabel label69;
        private DevExpress.XtraReports.UI.XRLabel label70;
        private DevExpress.XtraReports.UI.XRLabel label71;
        private DevExpress.XtraReports.UI.XRLabel label72;
        private DevExpress.XtraReports.UI.XRLabel label73;
        private DevExpress.XtraReports.UI.XRLabel label74;
        private DevExpress.XtraReports.UI.XRLabel label75;
        private DevExpress.XtraReports.UI.XRLabel label76;
        private DevExpress.XtraReports.UI.XRLabel label77;
        private DevExpress.XtraReports.UI.XRLabel label78;
        private DevExpress.XtraReports.UI.XRLabel label79;
        private DevExpress.XtraReports.UI.XRLabel label80;
        private DevExpress.XtraReports.UI.XRLabel label81;
        private DevExpress.XtraReports.UI.XRLabel label82;
        private DevExpress.XtraReports.UI.XRLabel label83;
        private DevExpress.XtraReports.UI.XRLabel label84;
        private DevExpress.XtraReports.UI.XRLabel label85;
        private DevExpress.XtraReports.UI.XRLabel label86;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRPanel panel5;
        private DevExpress.XtraReports.UI.XRLabel label87;
        private DevExpress.XtraReports.UI.XRLabel label88;
        private DevExpress.XtraReports.UI.XRLabel label89;
        private DevExpress.XtraReports.UI.XRLabel label90;
        private DevExpress.XtraReports.UI.XRLabel label91;
        private DevExpress.XtraReports.UI.XRLabel label92;
        private DevExpress.XtraReports.UI.XRLabel label93;
        private DevExpress.XtraReports.UI.XRLabel label94;
        private DevExpress.XtraReports.UI.XRLabel label95;
        private DevExpress.XtraReports.UI.XRLabel label96;
        private DevExpress.XtraReports.UI.XRLabel label97;
        private DevExpress.XtraReports.UI.XRLabel label98;
        private DevExpress.XtraReports.UI.XRLabel label99;
        private DevExpress.XtraReports.UI.XRLabel label100;
        private DevExpress.XtraReports.UI.XRLabel label101;
        private DevExpress.XtraReports.UI.XRLabel label102;
        private DevExpress.XtraReports.UI.XRLabel label103;
        private DevExpress.XtraReports.UI.XRLabel label104;
        private DevExpress.XtraReports.UI.XRLabel label105;
        private DevExpress.XtraReports.UI.XRLabel label106;
    }
}
